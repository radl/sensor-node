#include <Arduino.h>

#include <RADL.h>
#include <RGBLed.h>
#include <Button.h>

#define LED_R_PIN 11
#define LED_G_PIN 10
#define LED_B_PIN 9

#define BUTTON_PIN 12

#define CO2_LEVEL_PIN A2
//#define SOUND_LEVEL_PIN A3
#define BATTERY_LEVEL_PIN A4
#define LIGHT_INTENSITY_PIN A1
#define MOTION_DETECTOR_PIN 13
#define TEMPERATURE_HUMIDITY_PIN A0

// RGB Led for displaying device state
RGBLed led(LED_R_PIN, LED_G_PIN, LED_B_PIN);

// Button to interrupt BLE connection
Button button(BUTTON_PIN);

// Initialise all services
DeviceInformation deviceInformation(BATTERY_LEVEL_PIN);
EnvironmentReading environmentReadingSlow(EnvironmentReading::SLOW);
EnvironmentReading environmentReadingFast(EnvironmentReading::FAST);
EnvironmentEvent environmentEvent;

//Initialise all sensors
CO2Level co2Level(CO2_LEVEL_PIN);
//SoundLevel soundLevel(CO2_LEVEL_PIN);
LightIntensity lightItensity(LIGHT_INTENSITY_PIN);
MotionDetector motionDetector(MOTION_DETECTOR_PIN);
Temperature temperature(TEMPERATURE_HUMIDITY_PIN);
Humidity humidity(TEMPERATURE_HUMIDITY_PIN);

void setup() {
    // Setup Serial
    // Uncomment for debugging with Serial.println
    // Serial.begin(9600);
    // while (!Serial);

    led.display(RGBLed::GREEN);

    // Setup BLE
    if (!BLE.begin()) while (1);

    // Set advertising data
    BLE.setLocalName("RADL - SN");
    BLE.setAdvertisedServiceUuid(deviceInformation.uuid());

    // Add temperature and humidity calibration for CO2 sensing
    co2Level.setCalibration(&temperature, &humidity);

    // Link all sensors to their respective service
    environmentReadingSlow.link(temperature);
    environmentReadingSlow.link(humidity);
    environmentReadingSlow.link(co2Level);

    environmentReadingFast.link(lightItensity);
    //environmentReadingFast.link(soundLevel);

    environmentEvent.link(motionDetector);

    // Link all the services to BLE
    BLE.addService(deviceInformation.service());
    BLE.addService(environmentReadingSlow.service());
    BLE.addService(environmentReadingFast.service());
    BLE.addService(environmentEvent.service());

    // Start advertising
    BLE.advertise();
}

void loop() {
    BLEDevice central = BLE.central();

    if (central) {
        BLE.stopAdvertise();

        // Steady BLUE when BLE connected (MAGENTA if low battery)
        if (deviceInformation.batteryLevel().lowBattery()) led.display(RGBLed::MAGENTA);
        else led.display(RGBLed::BLUE);

        while (central.connected()) {
            // Check button, disconnect BLE if pressed
            if (button.pressed()) central.disconnect();
            else {
                // Loop the update of all the observers
                // while connected to a BLE central device
                UpdateHandler::instance().update();
            }
        }

        BLE.advertise();
    } else {
        // Blink BLUE when waiting for BLE connection (MAGENTA if low battery)
        if (led.updatedSinceMoreThan(500)) {
            if (led.currentState() != RGBLed::BLACK) led.display(RGBLed::BLACK);
            else {
                if (deviceInformation.batteryLevel().lowBattery()) led.display(RGBLed::MAGENTA);
                else led.display(RGBLed::BLUE);
            }
        }
    }
}
