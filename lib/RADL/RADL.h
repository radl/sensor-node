#ifndef _MY_LIB_H_
#define _MY_LIB_H_

#include "UpdateHandler/UpdateHandler.h"
#include "services/DeviceInformation/DeviceInformation.h"
#include "services/EnvironmentReading/EnvironmentReading.h"
#include "services/EnvironmentEvent/EnvironmentEvent.h"
#include "characteristics/Sensor/instances/sensors.h"

#endif
