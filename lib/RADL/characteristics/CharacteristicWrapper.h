#ifndef _CHARACTERISTIC_WRAPPER_H_
#define _CHARACTERISTIC_WRAPPER_H_

#include <ArduinoBLE.h>

#include "../utils/uuid/UUIDWrapper.h"

/**
 * @class CharacteristicWrapper
 * @brief Wrapper class for BLECharacteristic
 */
class CharacteristicWrapper : public UUIDWrapper {

    public:
        /**
         * @brief Inherit constructors from UUIDWrapper
         */
        using UUIDWrapper::UUIDWrapper;

        /**
         * @brief Getter for BLECharacteristic
         * 
         * @details To be implemented in derived class
         * 
         * @return Reference to a BLECharacteristic
         */
        virtual BLECharacteristic& characteristic() const = 0;
};

#endif
