#ifndef _UPDATE_FREQUENCY_H_
#define _UPDATE_FREQUENCY_H_

#include <ArduinoBLE.h>

#include "../CharacteristicWrapper.h"
#include "../../utils/update/notify/NotifyUpdatable.h"
#include "../../utils/update/publish/PublishUpdater.h"

/**
 * @class UpdateFrequency
 * @brief UpdateFrequency characteristic
 * 
 * @details Characteristic used in EnvironmentReading service
 *          Define a frequency for updating the reading sensors
 *          Subscribe to UpdateHandler for update from the BLE central device
 *          Publish any new frequency to the observers
 */
class UpdateFrequency : public CharacteristicWrapper, public NotifyUpdatable, public PublishUpdater<unsigned long> {

    public:
        /**
         * @brief Use main constructor with default value of 0
         */
        UpdateFrequency();

        /**
         * @brief Main constructor
         * 
         * @details Create BLE Characteristic
         *          The BLE central device can override the frequency value
         *          Subscribe to UpdateHandler for updates
         * 
         * @param value Initial frequency value
         */
        UpdateFrequency(unsigned long value);

        /**
         * @brief Destructor for deleting fields with pointer
         */
        virtual ~UpdateFrequency();

        /**
         * @brief Setter for frequency value
         * 
         * @details Write the value in the BLE Characteristic
         *          Plays the role of Subject from the observer pattern
         *          Publish the new frequency value to the observers
         * 
         * @param other Reference to the object to copy
         */
        void setValue(unsigned long value);

        /**
         * @brief Getter for the frequency value
         * 
         * @details Reads the value directly from the BLE Characteristic
         * 
         * @return Update frequency value
         */
        unsigned long value();

        /**
         * @brief Getter for BLECharacteristic (defined in CharacteristicWrapper)
         * 
         * @return Reference to the BLECharacteristic
         */
        BLECharacteristic& characteristic() const;

        /**
         * @brief Update method from observer pattern
         * 
         * @details Implement method defined in NotifyUpdatable
         */
        void update();

    private:
        BLETypedCharacteristic<unsigned long>* _pCharacteristic; // Pointer the the BLECharacteristic
};

#endif
