#include "UpdateFrequency.h"

#include "UpdateHandler/UpdateHandler.h"

UpdateFrequency::UpdateFrequency() : UpdateFrequency::UpdateFrequency(0) { }

UpdateFrequency::UpdateFrequency(unsigned long value) : CharacteristicWrapper(UUID("FF01", UUID::CUSTOM)) {
    this->_pCharacteristic = new BLETypedCharacteristic<unsigned long>(this->uuid(), BLERead | BLEWrite);
    UpdateHandler::instance().subscribe(this);
    this->setValue(value);
}

UpdateFrequency::~UpdateFrequency() {
    delete this->_pCharacteristic;
}

void UpdateFrequency::setValue(unsigned long value) {
    this->_pCharacteristic->writeValue(value);
    this->publish(this->value());
}

unsigned long UpdateFrequency::value() {
    return this->_pCharacteristic->value();
}

BLECharacteristic& UpdateFrequency::characteristic() const {
    return *this->_pCharacteristic;
}

void UpdateFrequency::update() {
    if (this->_pCharacteristic->written()) this->publish(this->value());
}
