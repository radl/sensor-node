#include "Information.h"

Information::Information(UUID uuid, const String information) : CharacteristicWrapper(uuid) {
    this->_pCharacteristic = new BLEStringCharacteristic(this->uuid(), BLERead, information.length());
    this->_pCharacteristic->writeValue(information);
}

Information::~Information() {
    delete this->_pCharacteristic;
}

BLECharacteristic& Information::characteristic() const {
    return *this->_pCharacteristic;
}
