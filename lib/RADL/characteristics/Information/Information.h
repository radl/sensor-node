#ifndef _INFORMATION_H_
#define _INFORMATION_H_

#include "../CharacteristicWrapper.h"

/**
 * @class Information
 * @brief String BLE Characteristic
 * 
 * @details BLE Characteristic provides information as String value
 */
class Information : CharacteristicWrapper {

    public:
        /**
         * @brief Main constructor
         * 
         * @param uuid        UUID that defines the BLE Characteristic
         * @param information Message contained by the BLE Characteristic
         */
        Information(UUID uuid, const String information);

        /**
         * @brief Destructor for deleting fields with pointer
         */
        virtual ~Information();

        /**
         * @brief Getter for BLECharacteristic (defined in CharacteristicWrapper)
         * 
         * @return Reference to the BLECharacteristic
         */
        BLECharacteristic& characteristic() const;

    private:
        BLEStringCharacteristic* _pCharacteristic; // Pointer to the BLECharacteristic
};

#endif
