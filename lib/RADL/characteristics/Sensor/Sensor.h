#ifndef _SENSOR_H_
#define _SENSOR_H_

#include <ArduinoBLE.h>

#include "../../utils/update/notify/NotifyUpdatable.h"
#include "../../UpdateHandler/UpdateHandler.h"
#include "../CharacteristicWrapper.h"
#include "PinReader/PinReader.h"

/**
 * @class Sensor
 * @brief Sensor BLE Characteristic
 * 
 * @details Base for Sensor BLE Characteristic
 *          Subscribe to UpdateHandler for updates (observer pattern)
 *          Use strategy pattern with PinReader as there are different methods for reading raw sensor value
 * 
 * @tparam T Data type of the sensor value
 */
template <typename T> class Sensor: public CharacteristicWrapper, public NotifyUpdatable {

    public:
        /**
         * @brief Main constructor
         * 
         * @details Create BLE Characteristic
         *          Subscribe to UpdateHandler for updates
         * 
         * @param uuid         UUID that defines the sensor as a BLE Characteristic
         * @param pin          Pin to use for reading the sensor value
         * @param initialValue Initial value of the sensor
         */
        Sensor(UUID uuid, byte pin, T initialValue);

        /**
         * @brief Destructor for deleting fields with pointer
         */
        virtual ~Sensor();

        /**
         * @brief Getter for the pin
         * 
         * @return Pin used for sensor reading
         */
        byte pin() const;

        /**
         * @brief Getter for the sensor value
         * 
         * @return Formatted sensor value
         */
        T value() const;

        /**
         * @brief Getter for BLECharacteristic (defined in CharacteristicWrapper)
         * 
         * @return Reference to the BLECharacteristic
         */
        BLECharacteristic& characteristic() const;

        /**
         * @brief Update method from observer pattern
         * 
         * @details Implement method defined in NotifyUpdatable
         */
        virtual void update();

    protected:
        /**
         * @brief Getter for the PinReader
         * 
         * @details Use of the strategy pattern
         *          To be implemented in derived class
         *          Implementation will use a (concrete) derived class of PinReader
         *          PinReader is then used to read the sensor value
         * 
         * @return Reference to the PinReader to use
         */
        virtual const PinReader& pinReader() const = 0;

        /**
         * @brief Format raw sensor value to the sensor data type
         * 
         * @details To be implemented in derived class
         * 
         * @param value Raw sensor value
         * 
         * @return Formatted value
         */
        virtual T format(word value) const = 0;

    private:
        BLETypedCharacteristic<T>* _pCharacteristic; // Pointer to the BLECharacteristic
        byte _pin;                                   // pin used for reading the sensor value
};

template <typename T> Sensor<T>::Sensor(UUID uuid, byte pin, T initialValue) : CharacteristicWrapper(uuid) {
    this->_pCharacteristic = new BLETypedCharacteristic<T>(this->uuid(), BLERead | BLENotify);
    this->_pin = pin;

    this->_pCharacteristic->writeValue(initialValue);

    UpdateHandler::instance().subscribe(this);
}

template <typename T> Sensor<T>::~Sensor() {
    delete this->_pCharacteristic;
}

template <typename T> byte Sensor<T>::pin() const {
    return this->_pin;
}

template <typename T> T Sensor<T>::value() const {
    word reading = this->pinReader().read();
    return this->format(reading);
}

template <typename T> BLECharacteristic& Sensor<T>::characteristic() const {
    return *this->_pCharacteristic;
}

template <typename T> void Sensor<T>::update() {
    T value = this->value();

    this->_pCharacteristic->writeValue(value);
}

#endif
