#include "PinReader.h"

PinReader::PinReader(byte pin) : _pin(pin) { }

byte PinReader::pin() const {
    return this->_pin;
}
