#ifndef _PIN_READER_H_
#define _PIN_READER_H_

#include <Arduino.h>

/**
 * @class PinReader
 * @brief Base class for pin readers
 * 
 * @details Used in strategy pattern with Sensor
 *          Sensor instances use derived classes from PinReader
 */
class PinReader {

    public:
        /**
         * Main constructor
         * 
         * @param pin Pin to use for reading sensor value
         */
        PinReader(byte pin);

        /**
         * @brief Getter for the pin
         * 
         * @return Pin value
         */
        byte pin() const;

        /**
         * @brief Method called to read raw sensor value
         * 
         * @return Raw sensor value
         */
        virtual word read() const = 0;

    private:
        byte _pin; // Pin used for reading sensor value
};

#endif
