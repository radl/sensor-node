#ifndef _DIGITAL_PIN_READER_H_
#define _DIGITAL_PIN_READER_H_

#include "../PinReader.h"

/**
 * @class DigitalPinReader
 * @brief Digital Pin Reader
 * 
 * @details Digital pin reader derived from PinReader
 *          Used in strategy pattern with Sensor instances
 */
class DigitalPinReader: public PinReader {

    public:
        /**
         * Main constructor
         * 
         * @param pin Pin to use for reading sensor value
         */
        DigitalPinReader(byte pin);

        /**
         * @brief Method called to read raw sensor value
         * 
         * @details Use Arduino digitalRead
         * 
         * @return Raw sensor value
         */
        word read() const;
};

#endif
