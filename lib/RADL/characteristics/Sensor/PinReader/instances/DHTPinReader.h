#ifndef _DHT_PIN_READER_H_
#define _DHT_PIN_READER_H_

#include <DHT.h>

#include "../PinReader.h"

/**
 * @class DHTPinReader
 * @brief DHT Pin Reader
 * 
 * @details DHT pin reader derived from PinReader
 *          Used in strategy pattern with Sensor instances
 *          Used to read either temperature or humidity out of DHT11 sensor
 */
class DHTPinReader : public PinReader {

    public:
        /** 
         * @enum Type
         * @brief DHT Reading Type
         * 
         * @details Whether the reader is used for temperature or humidity
         */
        enum Type {
            TEMPERATURE,
            HUMIDITY
        };

        /**
         * Main constructor
         * 
         * @param pin  Pin to use for reading sensor value
         * @param type Whether to read the temperature or humidity
         */
        DHTPinReader(byte pin, DHTPinReader::Type type);

        /**
         * @brief Method called to read raw sensor value
         * 
         * @details Use DHT library to read sensor values
         * 
         * @return Raw sensor value
         */
        word read() const;

    private:
        DHTPinReader::Type _type; // Whether the reader is used for temperature or humidity

        /**
         * @brief Getter for DHT instance from DHT library
         * 
         * @details Instanciate and setup DHT only once
         *          Even across multiple instances of the DHTPinReader
         */
        DHT& getDHT() const;
};

#endif
