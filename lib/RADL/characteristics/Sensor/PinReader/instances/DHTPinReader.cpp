#include "DHTPinReader.h"

DHTPinReader::DHTPinReader(byte pin, DHTPinReader::Type type) : PinReader(pin) {
    this->_type = type;
}

DHT& DHTPinReader::getDHT() const {
    static DHT dht(this->pin(), DHT11);
    static bool init = false;

    if (init == false) {
        dht.begin();
        init = true;
    }

    return dht;
}

word DHTPinReader::read() const {
    float value;
    
    if (this->_type == DHTPinReader::TEMPERATURE) value = this->getDHT().readTemperature();
    else value = this->getDHT().readHumidity();

    if (isnan(value)) return 0;
    else return constrain(value, 0, 100);
}
