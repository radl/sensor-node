#include "DigitalPinReader.h"

DigitalPinReader::DigitalPinReader(byte pin) : PinReader(pin) {
    pinMode(this->pin(), INPUT);
}

word DigitalPinReader::read() const {
    return digitalRead(this->pin());
}
