#include "AnalogPinReader.h"

AnalogPinReader::AnalogPinReader(byte pin) : PinReader(pin) {
    pinMode(this->pin(), INPUT);
}

word AnalogPinReader::read() const {
    return analogRead(this->pin());
}
