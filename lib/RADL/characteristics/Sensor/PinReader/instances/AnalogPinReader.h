#ifndef _ANALOG_PIN_READER_H_
#define _ANALOG_PIN_READER_H_

#include "../PinReader.h"

/**
 * @class AnalogPinReader
 * @brief Analog Pin Reader
 * 
 * @details Analog pin reader derived from PinReader
 *          Used in strategy pattern with Sensor instances
 */
class AnalogPinReader: public PinReader {

    public:
        /**
         * Main constructor
         * 
         * @param pin Pin to use for reading sensor value
         */
       AnalogPinReader(byte pin);

        /**
         * @brief Method called to read raw sensor value
         * 
         * @details Use Arduino analogRead
         * 
         * @return Raw sensor value
         */
        word read() const;
};

#endif
