#ifndef _LIGHT_INTENSITY_H_
#define _LIGHT_INTENSITY_H_

#include "../ReadingSensor.h"

/**
 * @class LightIntensity
 * @brief Light Intensity - Reading Sensor Characteristic
 */
class LightIntensity: public ReadingSensor<word> {

    public:
        static const byte RESISTOR; // Resistor value used with the LDR

        /**
         * Main constructor
         * 
         * @param pin Pin to use for reading sensor value
         */
        LightIntensity(byte pin);

    protected:
        /**
         * @brief Getter for the PinReader
         * 
         * @details Use of the strategy pattern
         * 
         * @return Reference to an AnalogReader
         */
        const PinReader& pinReader() const;

        /**
         * @brief Format raw sensor value to Lux
         * 
         * @param value Raw sensor value
         * 
         * @return Formatted value in Lux
         */
        word format(word value) const;
};

#endif
