#include "CO2Level.h"

#include "../PinReader/instances/AnalogPinReader.h"
#include "../../../utils/analog/Analog.h"

#include "Temperature.h"
#include "Humidity.h"

const float CO2Level::RL = 1.f;
const float CO2Level::R0 = 6.f;
const float CO2Level::DEFAULT_PPM = 417.16f;
const float CO2Level::SCALING_FACTOR = 116.6020682f;
const float CO2Level::EXPONENT = -2.769034857f;

CO2Level::CO2Level(byte pin) : ReadingSensor<word>(UUID("EC03", UUID::CUSTOM), pin, 0) {
    this->_pTemperature = nullptr;
    this->_pTemperature = nullptr;
}

const PinReader& CO2Level::pinReader() const {
    static AnalogPinReader pinReader(this->pin());
    return pinReader;
}

byte CO2Level::getR0() const {
    word value = this->pinReader().read();
    float rs = this->getRs(value);

    return rs * pow((CO2Level::SCALING_FACTOR / CO2Level::DEFAULT_PPM), (1. / CO2Level::EXPONENT));
}

void CO2Level::setCalibration(const Temperature* pTemperature, const Humidity* pHumidity) {
    this->_pTemperature = pTemperature;
    this->_pHumidity = pHumidity;
}

float CO2Level::getRs(word value) const {
    return ((Analog::MAX_ANALOG / (float)value) - 1.) * CO2Level::RL;
}

float CO2Level::getCalibratedRs(word value) const {
    byte temperature = this->_pTemperature->value();
    byte humidity = this->_pHumidity->value();
    float rs = this->getRs(value);

    // return rs / (0.00035 * pow(temperature, 2) - 0.02718 * temperature + 1.39538 - (humidity - 33.) * 0.0018);
    return rs / (1.6979 - 0.012 * temperature - 0.00612 * humidity);
}

word CO2Level::format(word value) const {
    float rs;
    
    if (this->_pTemperature != nullptr && this->_pHumidity != nullptr) rs = this->getCalibratedRs(value);
    else rs = this->getRs(value);

    return CO2Level::SCALING_FACTOR * pow(rs / CO2Level::R0, CO2Level::EXPONENT);
}
