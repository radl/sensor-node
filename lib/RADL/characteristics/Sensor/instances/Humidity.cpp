#include "Humidity.h"

#include "../PinReader/instances/DHTPinReader.h"

Humidity::Humidity(byte pin) : ReadingSensor<byte>(UUID("EC02", UUID::CUSTOM), pin, 0) { }

const PinReader& Humidity::pinReader() const {
    static DHTPinReader pinReader(this->pin(), DHTPinReader::HUMIDITY);
    return pinReader;
}

byte Humidity::format(word value) const {
    return value;
}
