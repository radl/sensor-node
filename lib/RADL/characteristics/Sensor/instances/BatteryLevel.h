#ifndef _BATTERY_LEVEL_H_
#define _BATTERY_LEVEL_H_

#include <Arduino.h>

#include "../ReadingSensor.h"

/**
 * @class BatteryLevel
 * @brief Battery Level - Reading Sensor Characteristic
 */
class BatteryLevel : public ReadingSensor<byte> {

    public:
        /**
         * Main constructor
         * 
         * @param pin Pin to use for reading sensor value
         */
        BatteryLevel(byte pin);

        /**
         * @brief Indicator of low battery
         * 
         * @return True if battery level < 20%, otherwise False
         */
        bool lowBattery();

    protected:
        /**
         * @brief Getter for the PinReader
         * 
         * @details Use of the strategy pattern
         * 
         * @return Reference to an AnalogReader
         */
        const PinReader& pinReader() const;

        /**
         * @brief Format raw sensor value to percentage
         * 
         * @param value Raw sensor value
         * 
         * @return Formatted value in percent
         */
        byte format(word value) const;
};

#endif
