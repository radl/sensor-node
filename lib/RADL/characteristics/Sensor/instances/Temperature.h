#ifndef _TEMPERATURE_H_
#define _TEMPERATURE_H_

#include "../ReadingSensor.h"

/**
 * @class Temperature
 * @brief Temperature - Reading Sensor Characteristic
 */
class Temperature : public ReadingSensor<byte> {

    public:
        /**
         * Main constructor
         * 
         * @param pin Pin to use for reading sensor value
         */
        Temperature(byte pin);

    protected:
        /**
         * @brief Getter for the PinReader
         * 
         * @details Use of the strategy pattern
         * 
         * @return Reference to a DHTPinReader
         */
        const PinReader& pinReader() const;

        /**
         * @brief Format raw sensor value to degree celsius
         * 
         * @details As the DHT module is used, the value is already in degree celsius
         * 
         * @param value Raw sensor value
         * 
         * @return Formatted value in degree celsius
         */
        byte format(word value) const;
};

#endif
