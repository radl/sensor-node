#ifndef _MOTION_DETECTOR_H_
#define _MOTION_DETECTOR_H_

#include "../EventSensor.h"

/**
 * @class MotionDetector
 * @brief Motion Detector - Event Sensor Characteristic
 */
class MotionDetector : public EventSensor<bool> {

    public:
        /**
         * Main constructor
         * 
         * @param pin Pin to use for reading sensor value
         */
        MotionDetector(byte pin);
    
    protected:
        /**
         * @brief Getter for the PinReader
         * 
         * @details Use of the strategy pattern
         * 
         * @return Reference to a DigitalReader
         */
        const PinReader& pinReader() const;

        /**
         * @brief Format raw sensor value to bool
         * 
         * @param value Raw sensor value
         * 
         * @return Formatted value in bool
         */
        bool format(word value) const;
};

#endif
