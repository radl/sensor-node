#include "MotionDetector.h"

#include "../PinReader/instances/DigitalPinReader.h"

MotionDetector::MotionDetector(byte pin) : EventSensor<bool>(UUID("EC06", UUID::CUSTOM), pin, 0) { }

const PinReader& MotionDetector::pinReader() const {
    static DigitalPinReader pinReader(this->pin());
    return pinReader;
}

bool MotionDetector::format(word value) const {
    return value > 0;
}
