#ifndef _SOUND_LEVEL_H_
#define _SOUND_LEVEL_H_

#include "../ReadingSensor.h"

/**
 * @class SoundLevel
 * @brief Sound Level - Reading Sensor Characteristic
 */
class SoundLevel : public ReadingSensor<byte> {

    public:
        /**
         * Main constructor
         * 
         * @param pin Pin to use for reading sensor value
         */
        SoundLevel(byte pin);

    protected:
        /**
         * @brief Getter for the PinReader
         * 
         * @details Use of the strategy pattern
         * 
         * @return Reference to an AnalogReader
         */
        const PinReader& pinReader() const;

        /**
         * @brief Format raw sensor value to dB
         * 
         * @param value Raw sensor value
         * 
         * @return Formatted value in dB
         */
        byte format(word value) const;
};

#endif
