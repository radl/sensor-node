#ifndef _HUMIDITY_H_
#define _HUMIDITY_H_

#include "../ReadingSensor.h"

/**
 * @class Humidity
 * @brief Humidity - Reading Sensor Characteristic
 */
class Humidity : public ReadingSensor<byte> {

    public:
        /**
         * Main constructor
         * 
         * @param pin Pin to use for reading sensor value
         */
        Humidity(byte pin);

    protected:
        /**
         * @brief Getter for the PinReader
         * 
         * @details Use of the strategy pattern
         * 
         * @return Reference to a DHTPinReader
         */
        const PinReader& pinReader() const;

        /**
         * @brief Format raw sensor value to percentage
         * 
         * @details As the DHT module is used, the value is already in percentage
         * 
         * @param value Raw sensor value
         * 
         * @return Formatted value in percent
         */
        byte format(word value) const;
};

#endif
