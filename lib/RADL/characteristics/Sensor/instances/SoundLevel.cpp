#include "SoundLevel.h"

#include "../PinReader/instances/AnalogPinReader.h"

SoundLevel::SoundLevel(byte pin) : ReadingSensor<byte>(UUID("EC05", UUID::CUSTOM), pin, 0) { }

const PinReader& SoundLevel::pinReader() const {
    static AnalogPinReader pinReader(this->pin());
    return pinReader;
}

byte SoundLevel::format(word value) const {
    return 20 * log10(value);
}
