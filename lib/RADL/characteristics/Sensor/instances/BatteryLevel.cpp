#include "BatteryLevel.h"

#include "../../../utils//analog/Analog.h"
#include "../PinReader/instances/AnalogPinReader.h"

BatteryLevel::BatteryLevel(byte pin) : ReadingSensor<byte>(UUID("2A19", UUID::STANDARD), pin, 0) { }

const PinReader& BatteryLevel::pinReader() const {
    static AnalogPinReader pinReader(this->pin());
    return pinReader;
}

byte BatteryLevel::format(word value) const {
    byte level = Analog::toPercent(value);

    // Level = actual level of battery
    // Arduino Nano IoT requires more than 50%
    // So  50% - 100% ==> 0% - 100%
    return map(level < 50 ? 50 : level, 50, 100, 0, 100);
}

bool BatteryLevel::lowBattery() {
    return this->value() <= 20;
}
