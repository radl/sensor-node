#include "LightIntensity.h"

#include "../../../utils/analog/Analog.h"
#include "../PinReader/instances/AnalogPinReader.h"

const byte LightIntensity::RESISTOR = 10;

LightIntensity::LightIntensity(byte pin) : ReadingSensor<word>(UUID("EC04", UUID::CUSTOM), pin, 0) { }

const PinReader& LightIntensity::pinReader() const {
    static AnalogPinReader pinReader(this->pin());
    return pinReader;
}

word LightIntensity::format(word value) const {
    float voltage = Analog::toVolt(value);
    
    return (500 * (Analog::MAX_VOLTAGE - voltage)) / (LightIntensity::RESISTOR * voltage);
}
