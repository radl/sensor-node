#ifndef _CO2_LEVEL_H_
#define _CO2_LEVEL_H_

#include "../ReadingSensor.h"
#include "Temperature.h"
#include "Humidity.h"

/**
 * @class CO2Level
 * @brief CO2 Level - Reading Sensor Characteristic
 * 
 * Based on previous work from G.Krocker (Mad Frog Labs):
 * https://github.com/smilexth/MQ135
 */
class CO2Level : public ReadingSensor<word> {

    public:
        static const float RL;             // Resistor value for RL
        static const float R0;             // Resistor value for R0 (to be calibrated)
        static const float DEFAULT_PPM;    // Default CO2 level in open air
        static const float SCALING_FACTOR; // Scaling factor for ppm conversion
        static const float EXPONENT;       // Exponent factor for ppm conversion

        /**
         * Main constructor
         * 
         * @param pin Pin to use for reading sensor value
         */
        CO2Level(byte pin);

        /**
         * @brief Getter for R0 value
         * 
         * @details Used to calibrate R0
         *          To be used in open air after sensor burn-in
         *          Static field R0 needs to be changed to the value obtained
         * 
         * @return Value for R0
         */
        byte getR0() const;

        /**
         * @brief Setter for Temperature and Humidity sensor
         * 
         * @details Set the Temperature and Humidity sensor
         *          When defined, they are used to correct the value of Rs
         * 
         * @param pTemperature Pointer to a Temperature sensor
         * @param pHumidity    Pointer to a Humidity sensor
         */
        void setCalibration(const Temperature* pTemperature, const Humidity* pHumidity);

    protected:
        /**
         * @brief Getter for the PinReader
         * 
         * @details Use of the strategy pattern
         * 
         * @return Reference to an AnalogReader
         */
        const PinReader& pinReader() const;

        /**
         * @brief Format raw sensor value to PPM
         * 
         * @param value Raw sensor value
         * 
         * @return Formatted value in PPM
         */
        word format(word value) const;

    private:
        const Temperature* _pTemperature; // Pointer to Temperature sensor (used for calibration)
        const Humidity* _pHumidity;       // Pointer to Humidity sensor (used for calibration)

        /**
         * @brief Calculate Rs from analog value of RL
         * 
         * @param value Analog value from PinReader
         * 
         * @return Value of Rs
         */
        float getRs(word value) const;

        /**
         * @brief Correct value of calculated Rs given Temperature and Humidity values
         * 
         * @param value Analog value from PinReader
         * 
         * @return Corrected value of Rs
         */
        float getCalibratedRs(word value) const;
};

#endif
