#include "Temperature.h"

#include "../PinReader/instances/DHTPinReader.h"

Temperature::Temperature(byte pin) : ReadingSensor<byte>(UUID("EC01", UUID::CUSTOM), pin, 0) { }

const PinReader& Temperature::pinReader() const {
    static DHTPinReader pinReader(this->pin(), DHTPinReader::TEMPERATURE);
    return pinReader;
}

byte Temperature::format(word value) const {
    return value;
}
