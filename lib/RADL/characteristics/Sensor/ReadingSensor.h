#ifndef _READING_SENSOR_H_
#define _READING_SENSOR_H_

#include "Sensor.h"
#include "../../utils/update/instances/UpdatableValue.h"
#include "../UpdateFrequency/UpdateFrequency.h"

/**
 * @class ReadingSensor
 * @brief Sensor Reading BLE Characteristic
 * 
 * @details Derived from Sensor (Base for Sensor BLE Characteristic)
 *          Characteristic used in EnvironmentReading Service
 *          Update sensor value given a frequency
 * 
 * @tparam T Data type of the sensor value
 */
template <typename T> class ReadingSensor : public Sensor<T> {

    public:
        /**
         * @brief Main constructor
         * 
         * @details Calls Sensor constructor
         *          Initialise frequency
         * 
         * @param uuid         UUID that defines the sensor as a BLE Characteristic
         * @param pin          Pin to use for reading the sensor value
         * @param initialValue Initial value of the sensor
         */
        ReadingSensor(UUID uuid, byte pin, T initialValue);

        /**
         * @brief Destructor for deleting fields with pointer
         */
        virtual ~ReadingSensor();

        /**
         * @brief Setter for frequency
         * 
         * @details Use observer pattern to update sensor frequency
         *          (Observer) Sensor frequency subscribes to updates from given UpdateFrequency
         *          (Subject) UpdateFrequency publishes any new value when frequency changes
         * 
         * @param updateFrequency UpdateFrequency object from EnvironmentReading service
         */
        void setFrequency(UpdateFrequency* pUpdateFrequency);

        /**
         * @brief Setter for frequency
         * 
         * @param frequency Frequency of sensor update
         */
        void setFrequency(unsigned long frequency);

        /**
         * @brief Override method to add time constaints
         * 
         * @details Update only if frequency timeout reached
         */
        virtual void update();

    private:
        unsigned long _prevReadingTime;             // Previous reading time
        UpdatableValue<unsigned long>* _pFrequency; // Pointer to the updatable frequency
};

template <typename T> ReadingSensor<T>::ReadingSensor(UUID uuid, byte pin, T initialValue) : Sensor<T>::Sensor(uuid, pin, initialValue) {
    this->_pFrequency = new UpdatableValue<unsigned long>(0);
    this->_prevReadingTime = 0;
}

template <typename T> ReadingSensor<T>::~ReadingSensor() {
    delete this->_pFrequency;
}

template <typename T> void ReadingSensor<T>::setFrequency(UpdateFrequency* pUpdateFrequency) {
    this->_pFrequency->update(pUpdateFrequency->value());
    this->_prevReadingTime = millis() + pUpdateFrequency->value();
    pUpdateFrequency->subscribe(this->_pFrequency);
}

template <typename T> void ReadingSensor<T>::setFrequency(unsigned long frequency) {
    this->_prevReadingTime = millis() + frequency;
    this->_pFrequency->update(frequency);
}

template <typename T> void ReadingSensor<T>::update() {
    if (this->_pFrequency->value() == 0) return;
    else if ((millis() - this->_prevReadingTime) >= this->_pFrequency->value()) {
        this->_prevReadingTime = millis();
        Sensor<T>::update();

        // Adding delay between BLE notifications
        delay(250);
    }
}

#endif
