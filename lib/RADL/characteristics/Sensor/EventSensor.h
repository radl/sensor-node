#ifndef _EVENT_SENSOR_H_
#define _EVENT_SENSOR_H_

#include "Sensor.h"

/**
 * @class EventSensor
 * @brief Sensor Event BLE Characteristic
 * 
 * @details Derived from Sensor (Base for Sensor BLE Characteristic)
 *          Update sensor value as soon as it exceeds a given threshold compared to previous value
 *          Characteristic used in EnvironmentEvent Service
 * 
 * @tparam T Data type of the sensor value
 */
template <typename T> class EventSensor : public Sensor<T> {

    public:
        /**
         * @brief Main constructor
         * 
         * @details Calls Sensor constructor
         *          Initialise threshold
         * 
         * @param uuid         UUID that defines the sensor as a BLE Characteristic
         * @param pin          Pin to use for reading the sensor value
         * @param initialValue Initial value of the sensor
         */
        EventSensor(UUID uuid, byte pin, T initialValue);

        /**
         * @brief Setter for the threshold
         * 
         * @param threshold New threshold value
         */
        void setThreshold(float threshold);

        /**
         * @brief Override method to add threshold constaints
         * 
         * @details Update only if threshold compared to previous value is exceed
         */
        virtual void update();

    private:
        T _prevValue;     // Previous sensor value
        float _threshold; // Threshold
};

template <typename T> EventSensor<T>::EventSensor(UUID uuid, byte pin, T initialValue) : Sensor<T>::Sensor(uuid, pin, initialValue) {
    this->_prevValue = initialValue;
    this->_threshold = 0;
}

template <typename T> void EventSensor<T>::setThreshold(float threshold) {
    this->_threshold = abs(threshold);
}

template <typename T> void EventSensor<T>::update() {
    T newValue = this->value();

    if (abs(newValue - this->_prevValue) > this->_threshold) {
        this->_prevValue = newValue;
        Sensor<T>::update();
    }
}

#endif
