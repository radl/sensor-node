#include "Analog.h"

const word Analog::MIN_ANALOG = 0;
const word Analog::MAX_ANALOG = 1023;

const float Analog::MIN_VOLTAGE = 0;
const float Analog::MAX_VOLTAGE = 3.3f;

float Analog::toVolt(word value) {
    return (float)value / Analog::MAX_ANALOG * Analog::MAX_VOLTAGE;
}

byte Analog::toPercent(word value) {
    return map(value, Analog::MIN_ANALOG, Analog::MAX_ANALOG, 0, 100);
}
