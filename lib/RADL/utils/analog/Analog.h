#ifndef _ANALOG_H_
#define _ANALOG_H_

#include <Arduino.h>

/**
 * @class Analog
 * @brief Helper class for analog conversions
 */
class Analog {

    public:
        static const float MIN_VOLTAGE; // Minimum voltage
        static const float MAX_VOLTAGE; // Maximum voltage (operating voltage)
        
        static const word MIN_ANALOG; // Minimum analog value
        static const word MAX_ANALOG; // Maximum analog value

        /**
         * @brief Conversion of an analog value to Volt
         * 
         * @param value Analog value to convert
         * 
         * @return Value converted to Volt
         */
        static float toVolt(word value);

        /**
         * @brief Conversion of an analog value to Percentage
         * 
         * @param value Analog value to convert
         * 
         * @return Value converted to Percentage
         */
        static byte toPercent(word value);
};

#endif
