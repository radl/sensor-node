#ifndef _UUID_H_
#define _UUID_H_

/**
 * @class UUID
 * @brief UUID Maker
 * 
 * @details Creating standard or custom uuid according to Bluetooth standards
 */
class UUID {
    
    public:
        /** 
         * @enum Type
         * @brief UUID Type
         * 
         * @details Weither the UUID is standard or custom
         */
        enum Type {
            STANDARD,
            CUSTOM
        };

        /**
         * @brief Empty constructor
         * 
         * @details Creates default custom UUID (0000FFFF-9DED-11EA-AB12-0800200C9A66)
         */
        UUID();

        /**
         * @brief Main constructor
         * 
         * @details Makes a 128 bits ID from the 16 bits ID
         * 
         * |   Type   |               Base UUID              |
         * |:---------|:-------------------------------------|
         * | STANDARD | 0000xxxx-0000-1000-8000-00805F9B34FB |
         * | CUSTOM   | 0000xxxx-9DED-11EA-AB12-0800200C9A66 |
         * 
         * @param id   16 bits ID
         * @param type Weither the UUID is standard or custom
         */
        UUID(const char* id, UUID::Type type);

        /**
         * @brief Copy constructor
         * 
         * @param other Reference to the UUID object to copy
         */
        UUID(const UUID& other);

        /**
         * @brief Assignment operator
         * 
         * @param other Reference to the UUID object to copy
         * 
         * @return Reference to self
         */
        UUID& operator=(UUID const& other);

        /**
         * @brief Getter for UUID value
         * 
         * @return Constant char array containing the UUID value
         */
        const char* value() const;

        /**
         * @brief Getter for UUID type
         * 
         * @return UUID type
         */
        UUID::Type type() const;
    
    private:
        char _value[36+1]; // UUID (version 1) value
        UUID::Type _type;  // UUID type
};

#endif
