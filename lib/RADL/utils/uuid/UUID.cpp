#include "UUID.h"

#include <cstring>

UUID::UUID() : UUID::UUID("FFFF", UUID::CUSTOM) { }

UUID::UUID(const char* uuid_id, UUID::Type type) {
    strcpy(this->_value, "0000");
    strcat(this->_value, uuid_id);

    if (type == STANDARD) strcat(this->_value, "-0000-1000-8000-00805F9B34FB");
    else strcat(this->_value, "-9DED-11EA-AB12-0800200C9A66");

    this->_type = type;
}

UUID::UUID(const UUID& other) {
    strcpy(this->_value, other._value);
    this->_type = other._type;
}

UUID& UUID::operator=(const UUID& other) {
    strcpy(this->_value, other._value);
    this->_type = other._type;
    return *this;
}

const char* UUID::value() const {
    return this->_value;
}

UUID::Type UUID::type() const {
    return this->_type;
}
