#ifndef _UUID_WRAPPER_H_
#define _UUID_WRAPPER_H_

#include "../../utils/uuid/UUID.h"

/**
 * @class UUIDWrapper
 * @brief Class containing a UUID field
 */
class UUIDWrapper {
    
    public:
        /**
         * @brief Main constructor
         * 
         * @param uuid UUID object
         */
        UUIDWrapper(UUID uuid);

        /**
         * @brief Disable copy constructor
         * 
         * @param other Reference to the object to copy
         */
        UUIDWrapper(const UUIDWrapper& other) = delete;

        /**
         * @brief Disable assignment operator
         * 
         * @param other Reference to the object to copy
         */
        void operator=(UUIDWrapper const& other) = delete;

        /**
         * @brief Getter for UUID
         * 
         * @return Constant char array containing the UUID value
         */
        const char* uuid() const;

    private:
        UUID _uuid; // UUID object
};

#endif
