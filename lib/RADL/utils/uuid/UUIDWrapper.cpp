#include "UUIDWrapper.h"

UUIDWrapper::UUIDWrapper(UUID uuid) {
    this->_uuid = uuid;
}

const char* UUIDWrapper::uuid() const {
    return this->_uuid.value();
}
