#ifndef _NOTIFY_UPDATABLE_H_
#define _NOTIFY_UPDATABLE_H_

/**
 * @class NotifyUpdatable
 * @brief Observer from the observer pattern
 * 
 * @details Declaration of update method
 *          Subject will call this method to notify the observers to update
 *          Needs to be implemented in derived class
 */
class NotifyUpdatable {

    public:
        /**
         * @brief Update method to be implemented by the observers
         */
        virtual void update() = 0;
};

#endif
