#ifndef _NOTIFY_UPDATER_H_
#define _NOTIFY_UPDATER_H_

#include "../Provider.h"
#include "NotifyUpdatable.h"

/**
 * @class NotifyUpdater
 * @brief Subject from the observer pattern
 * 
 * @details Derived from base class Provider
 *          With method to notify the observers
 *          Observers type is NotifyUpdatble
 */
class NotifyUpdater : public Provider<NotifyUpdatable> {

    public:
        /**
         * @brief Simple notification for the observers to update
         */
        void notify();
};

#endif
