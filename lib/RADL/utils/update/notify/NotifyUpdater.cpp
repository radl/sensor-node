#include "NotifyUpdater.h"

void NotifyUpdater::notify() {
    for (byte i = 0; i < this->_subscribers.size(); i++) {
        this->_subscribers.at(i)->update();
    }
}
