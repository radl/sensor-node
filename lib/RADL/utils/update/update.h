#ifndef _UPDATE_H_
#define _UPDATE_H_

#include "Provider.h"

#include "notify/NotifyUpdatable.h"
#include "notify/NotifyUpdater.h"

#include "publish/PublishUpdatable.h"
#include "publish/PublishUpdater.h"

#include "instances/UpdatableValue.h"

#endif
