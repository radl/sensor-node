#ifndef _PROVIDER_H_
#define _PROVIDER_H_

#include <Array.h>

#define MAX_SUBSCRIBERS 10

/**
 * @class Provider
 * @brief Base class for a subject from the observer pattern
 * 
 * @details Container for a list of observers
 *          With methods to subscribe and unsubscribe
 *          Maximum of 10 observers
 * 
 * @tparam T Type of the observers from the observer pattern
 */
template <typename T> class Provider {

    public:
        /**
         * @brief Method to subscribe
         * 
         * @param pObject Pointer to the observer that is subscribing
         */
        void subscribe(T* pObject);

        /**
         * @brief Method to unsubscribing
         * 
         * @param pObject Pointer to the oberserver that is unsubscribing
         */
        void unsubscribe(T* pObject);
    
    protected:
        Array<T*, MAX_SUBSCRIBERS> _subscribers; // List of observers
};

template <typename T> void Provider<T>::subscribe(T* pObject) {
    if (this->_subscribers.size() < MAX_SUBSCRIBERS) this->_subscribers.push_back(pObject);
}

template <typename T> void Provider<T>::unsubscribe(T* pObject) {
    for (byte i = 0; i < this->_subscribers.size(); i++) {
        if (this->_subscribers.at(i) == pObject) {
            this->_subscribers.remove(i);
            break;
        }
    }
}

#endif
