#ifndef _UPDATABLE_VALUE_H_
#define _UPDATABLE_VALUE_H_

#include "../publish/PublishUpdatable.h"

/**
 * @class UpdatableValue
 * @brief Container of a value, updated via the observer pattern
 * 
 * @details Container of an updatable value
 *          Update the value by playing the role of Observer from the observer pattern
 *          Needs to subscribe to a PublishUpdater
 * 
 * @tparam T Data type of the value
 */
template <typename T> class UpdatableValue : public PublishUpdatable<T> {

    public:
        /**
         * @brief Main constructor
         * 
         * @param initialValue Initial value
         */
        UpdatableValue(T initialValue);

        /**
         * @brief Disable copy constructor
         * 
         * @param other Reference to the object to copy
         */
        UpdatableValue(const UpdatableValue& other) = delete;

        /**
         * @brief Disable assignment operator
         * 
         * @param other Reference to the object to copy
         */
        void operator=(UpdatableValue const& other) = delete;

        /**
         * @brief Destructor
         */
        virtual ~UpdatableValue();

        /**
         * @brief Update method from observer pattern
         * 
         * @details Implement method defined in PublishUpdatable
         * 
         * @param data Data transmitted by the Subject (PublishUpdater)
         */
        void update(T data);

        /**
         * @brief Getter for the value
         * 
         * @return The value
         */
        T value();

    private:
        T _value; // Value contained and updated by the class
};

template <typename T> UpdatableValue<T>::UpdatableValue(T initialValue) {
    this->_value = initialValue;
}

template <typename T> UpdatableValue<T>::~UpdatableValue() { }

template <typename T> T UpdatableValue<T>::value() {
    return this->_value;
}

template <typename T> void UpdatableValue<T>::update(T data) {
    this->_value = data;
}

#endif
