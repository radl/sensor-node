#ifndef _PUBLISH_UPDATABLE_H_
#define _PUBLISH_UPDATABLE_H_

/**
 * @class PublishUpdatable
 * @brief Observer from the observer pattern
 * 
 * @details Declaration of a publish method
 *          Subject will call this method to transmit data to the observers
 *          Needs to be implemented in derived class
 * 
 * @tparam T Type of data transmitted to the observers
 */
template <typename T> class PublishUpdatable {

    public:
        /**
         * @brief Update method to be implemented by the observers
         * 
         * @param data Data transmitted to the oberservers
         */
        virtual void update(T data) = 0;
};

#endif
