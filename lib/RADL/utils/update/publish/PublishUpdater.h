#ifndef _PUBLISH_UPDATER_H_
#define _PUBLISH_UPDATER_H_

#include "../Provider.h"
#include "PublishUpdatable.h"

/**
 * @class PublishUpdater
 * @brief Subject from the observer pattern
 * 
 * @details Derived from base class Provider
 *          With method to publish data to the observers
 *          Observers type is PublishUpdatble
 * 
 * @tparam T Type of data transmitted to the observers
 */
template <typename T> class PublishUpdater : public Provider<PublishUpdatable<T>> {

    public:
        /**
         * @brief Transmit data to the observers
         * 
         * @param data Data to transmit
         */
        void publish(T data);
};

template <typename T> void PublishUpdater<T>::publish(T data) {
    for (byte i = 0; i < this->_subscribers.size(); i++) {
        this->_subscribers.at(i)->update(data);
    }
}

#endif
