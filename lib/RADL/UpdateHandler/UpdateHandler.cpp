#include "UpdateHandler.h"

#include <ArduinoBLE.h>

UpdateHandler::UpdateHandler() { }

UpdateHandler& UpdateHandler::instance() {
    static UpdateHandler instance;
    return instance;
}

void UpdateHandler::update() {
    BLE.poll();
    this->notify();
}
