#ifndef _UPDATE_HANDLER_H_
#define _UPDATE_HANDLER_H_

#include "utils/update/notify/NotifyUpdater.h"

/**
 * @class UpdateHandler
 * @brief Handle the update of subscribers as long as connected to a BLE central device
 * 
 * @details Plays the role of Subject from the observer pattern
 *          Update continuously a list of observers as long as connected to a BLE central device
 *          Use the singleton pattern, as it requires to be unique
 */
class UpdateHandler : public NotifyUpdater {

    public:
        /**
         * @brief Disable copy constructor
         * 
         * @param other Reference to the object to copy
         */
        UpdateHandler(UpdateHandler const&) = delete;

        /**
         * @brief Disable assignment operator
         * 
         * @param other Reference to the object to copy
         */
        void operator=(UpdateHandler const&) = delete;

        /**
         * @brief Getter for the UpdateHandler instance
         * 
         * @details Used for singleton pattern, allowing only one instance
         * 
         * @param The UpdateHandler instance
         */
        static UpdateHandler& instance();

        /**
         * @brief Handle the updates
         * 
         * @details Update continuously a list of observers (NotifyUpdatable)
         *          Exit loop when disconnected from the BLE central device
         *          Poll BLE events before each iterations
         */
        void update();

    private:
        /**
         * @brief Main constructor (private)
         */
        UpdateHandler();
};

#endif
