#include "EnvironmentEvent.h"

EnvironmentEvent::EnvironmentEvent() : ServiceWrapper(UUID("EE04", UUID::CUSTOM)) {
    this->_pService = new BLEService(this->uuid());
}

EnvironmentEvent::~EnvironmentEvent() {
    delete this->_pService;
}

BLEService& EnvironmentEvent::service() const {
    return *this->_pService;
}
