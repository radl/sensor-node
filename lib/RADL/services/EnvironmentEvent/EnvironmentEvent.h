#ifndef _ENVIRONMENT_EVENT_H_
#define _ENVIRONMENT_EVENT_H_

#include <ArduinoBLE.h>

#include "../../characteristics/Sensor/EventSensor.h"
#include "../ServiceWrapper.h"
#include "../../characteristics/CharacteristicWrapper.h"

/**
 * @class EnvironmentEvent
 * @brief BLE Service providing readings from event sensors
 * 
 * @details Provides readings from event sensors
 *          The number of sensors used is flexible
 *          EventSensor instances can be added to the service using link method
 */
class EnvironmentEvent : public ServiceWrapper {
    
    public:
        /**
         * @brief Main constructor
         */
        EnvironmentEvent();

        /**
         * @brief Destructor for deleting fields with pointer
         */
        ~EnvironmentEvent();

        /**
         * @brief Getter for BLEService (defined in ServiceWrapper)
         * 
         * @return Reference to the BLEService
         */
        BLEService& service() const;

        /**
         * @brief Link an event sensor to the BLE Service
         * 
         * @return sensor Any instance of EventSensor
         */
        template <typename T> void link(EventSensor<T>& sensor);

    private:
        BLEService* _pService; // Pointer to the BLEService
};

template <typename T> void EnvironmentEvent::link(EventSensor<T>& sensor) {
    this->_pService->addCharacteristic(sensor.characteristic());
}

#endif
