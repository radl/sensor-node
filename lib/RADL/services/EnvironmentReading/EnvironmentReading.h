#ifndef _ENVIRONMENT_SENSING_SERIVCE_H_
#define _ENVIRONMENT_SENSING_SERVICE_H_

#include <ArduinoBLE.h>

#include "../ServiceWrapper.h"
#include "../../characteristics/Sensor/ReadingSensor.h"

/**
 * @class EnvironmentReading
 * @brief BLE Service providing regular readings from environmental sensors
 * 
 * @details Provides regular readings from environmental sensors
 *          Use a characteristic for the update frequency (writable by BLE central device)
 *          ReadinSensor instances can be added to the service using link method
 *          The number of sensors used is flexible
 */
class EnvironmentReading : public ServiceWrapper {

    public:
        /** 
         * @enum Frequency
         * @brief Update frequency
         * 
         * @details Different update frequency values
         */
        enum Frequency {
            SLOW = 5000,
            FAST = 1000
        };

        /**
         * @brief Main constructor
         * 
         * @details Create an UpdateFrequency characteristic and add it to the BLE Service
         *          Added sensors will be linked to this UpdateFrequency characteristic
         *          Use a default frequency, but can be overriden by the BLE central device
         * 
         * @param frequency Default frequency for updating sensor readings
         */
        EnvironmentReading(EnvironmentReading::Frequency frequency);

        /**
         * @brief Destructor for deleting fields with pointer
         */
        ~EnvironmentReading();

        /**
         * @brief Getter for BLEService (defined in ServiceWrapper)
         * 
         * @return Reference to the BLEService
         */
        BLEService& service() const;

        /**
         * @brief Link a reading sensor to the BLE Service
         * 
         * @return sensor Any instance of ReadingSensor
         */
        template <typename T> void link(ReadingSensor<T>& sensor);

    private:
        BLEService* _pService;            // Pointer to the BLEService
        UpdateFrequency* _pUpdateFrequency; // UpdateFrequency characteristic
};

template <typename T> void EnvironmentReading::link(ReadingSensor<T>& sensor) {
    this->_pService->addCharacteristic(sensor.characteristic());
    sensor.setFrequency(this->_pUpdateFrequency);
}

#endif
