#include "EnvironmentReading.h"

// Slow update frequency ==> UUID (EE02)
// Fast update frequency ==> UUID (EE03)
EnvironmentReading::EnvironmentReading(EnvironmentReading::Frequency frequency) : ServiceWrapper(UUID(frequency == EnvironmentReading::SLOW ? "EE02" : "EE03", UUID::CUSTOM)) {
    this->_pUpdateFrequency = new UpdateFrequency(frequency);

    this->_pService = new BLEService(this->uuid());
    this->_pService->addCharacteristic(this->_pUpdateFrequency->characteristic());
}

EnvironmentReading::~EnvironmentReading() {
    delete this->_pService;
    delete this->_pUpdateFrequency;
}

BLEService& EnvironmentReading::service() const {
    return *this->_pService;
}
