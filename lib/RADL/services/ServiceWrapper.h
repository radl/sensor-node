#ifndef _SERVICE_WRAPPER_H_
#define _SERVICE_WRAPPER_H_

#include <ArduinoBLE.h>

#include "../utils/uuid/UUIDWrapper.h"

/**
 * @class ServiceWrapper
 * @brief Wrapper class for BLEService
 */
class ServiceWrapper : public UUIDWrapper {

    public:
        /**
         * @brief Inherit constructors from UUIDWrapper
         */
        using UUIDWrapper::UUIDWrapper;

        /**
         * @brief Getter for BLEService
         * 
         * @details To be implemented in derived class
         * 
         * @return Reference to a BLEService
         */
        virtual BLEService& service() const = 0;
};

#endif
