#ifndef _DEVICE_INFORMATION_H_
#define _DEVICE_INFORMATION_H_

#include "../ServiceWrapper.h"
#include "../../characteristics/Information/Information.h"
#include "../../characteristics/Sensor/instances/BatteryLevel.h"

/**
 * @class DeviceInformation
 * @brief BLE Service providing device name, serial number and battery level
 */
class DeviceInformation : public ServiceWrapper {

    public:
        /**
         * @brief Main constructor
         * 
         * @param batteryLevelPin Pin to use for reading battery level
         */
        DeviceInformation(byte batteryLevelPin);

        /**
         * @brief Destructor for deleting fields with pointer
         */
        ~DeviceInformation();

        /**
         * @brief Getter for BLEService (defined in ServiceWrapper)
         * 
         * @return Reference to the BLEService
         */
        BLEService& service() const;

        /**
         * @brief Getter for the BatteryLevel instance
         * 
         * @return Reference to the BatteryLevel
         */
        BatteryLevel& batteryLevel() const;

    private:
        BLEService* _pService;        // Pointer to the BLEService

        Information* _pDeviceName;    // Pointer to the device name characteristic
        Information* _pSerialNumber;  // Pointer to the serial number characteristic
        BatteryLevel* _pBatteryLevel; // Pointer to the battery level characteristic
};

#endif
