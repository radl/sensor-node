#include "DeviceInformation.h"

DeviceInformation::DeviceInformation(byte batteryLevelPin) : ServiceWrapper(UUID("EE01", UUID::CUSTOM)) {
    this->_pService = new BLEService(this->uuid());

    this->_pDeviceName = new Information(UUID("2A00", UUID::STANDARD), "RADL - Sensor Node");
    this->_pSerialNumber = new Information(UUID("2A25", UUID::STANDARD), "RADL-SN-0001");
    this->_pBatteryLevel = new BatteryLevel(batteryLevelPin);

    this->_pBatteryLevel->setFrequency(30000);

    this->_pService->addCharacteristic(this->_pDeviceName->characteristic());
    this->_pService->addCharacteristic(this->_pSerialNumber->characteristic());
    this->_pService->addCharacteristic(this->_pBatteryLevel->characteristic());
}

DeviceInformation::~DeviceInformation() {
    delete this->_pService;
    delete this->_pDeviceName;
    delete this->_pSerialNumber;
    delete this->_pBatteryLevel;
}

BLEService& DeviceInformation::service() const {
    return *this->_pService;
}

BatteryLevel& DeviceInformation::batteryLevel() const {
    return *this->_pBatteryLevel;
}
