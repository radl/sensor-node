#include "RGBLed.h"

RGBLed::RGBLed(byte r_pin, byte g_pin, byte b_pin) {
    this->_lastUpdated = 0;
    this->_r_pin = r_pin;
    this->_g_pin = g_pin;
    this->_b_pin = b_pin;

    pinMode(this->_r_pin, OUTPUT);
    pinMode(this->_g_pin, OUTPUT);
    pinMode(this->_b_pin, OUTPUT);

    this->display(RGBLed::BLACK);
}

void RGBLed::display(unsigned char r_value, unsigned char g_value, unsigned char b_value) {
    analogWrite(this->_r_pin, r_value);
    analogWrite(this->_g_pin, g_value);
    analogWrite(this->_b_pin, b_value);

    this->_lastUpdated = millis();
}

void RGBLed::display(RGBLed::Colour colour) {
    switch (colour) {
        case RGBLed::RED:
            this->display(255, 0, 0);
            break;
        
        case RGBLed::ORANGE:
            this->display(255, 127, 0);
            break;
        
        case RGBLed::YELLOW:
            this->display(255, 255, 0);
            break;
        
        case RGBLed::CHARTREUSE:
            this->display(127, 255, 0);
            break;
        
        case RGBLed::GREEN:
            this->display(0, 255, 0);
            break;
        
        case RGBLed::AQUAMARINE:
            this->display(0, 255, 127);
            break;
        
        case RGBLed::CYAN:
            this->display(0, 255, 255);
            break;
        
        case RGBLed::AZURE:
            this->display(0, 127, 255);
            break;
        
        case RGBLed::BLUE:
            this->display(0, 0, 255);
            break;
        
        case RGBLed::VIOLET:
            this->display(127, 0, 255);
            break;
        
        case RGBLed::MAGENTA:
            this->display(255, 0, 255);
            break;
        
        case RGBLed::ROSE:
            this->display(255, 0, 127);
            break;
        
        case RGBLed::WHITE:
            this->display(255, 255, 255);
            break;
        
        default:
            this->display(0, 0, 0);
            break;
    }

    this->_currentState = colour;
}

RGBLed::Colour RGBLed::currentState() {
    return this->_currentState;
}

bool RGBLed::updatedSinceMoreThan(unsigned long value) {
    return (millis() - this->_lastUpdated) >= value;
}
