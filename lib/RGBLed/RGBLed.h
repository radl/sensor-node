#ifndef RGB_LED_H
#define RGB_LED_H

#include <Arduino.h>

/**
 * @class RGBLed
 * @brief Helper class for RGB Led controls
 */
class RGBLed {

    public:
        /** 
         * @enum Colour
         * @brief RGB Colour
         * 
         * @details Supported RGB colours for the LED
         */
        enum Colour {
            RED        = 0,  // 255,   0,   0
            ORANGE     = 1,  // 255, 127,   0
            YELLOW     = 2,  // 255, 255,   0
            CHARTREUSE = 3,  // 127, 255,   0
            GREEN      = 4,  //   0, 255,   0
            AQUAMARINE = 5,  //   0, 255, 127
            CYAN       = 6,  //   0, 255, 255
            AZURE      = 7,  //   0, 127, 255
            BLUE       = 8,  //   0,   0, 255
            VIOLET     = 9,  // 127,   0, 255
            MAGENTA    = 10, // 255,   0, 255
            ROSE       = 11, // 255,   0, 127
            WHITE      = 12, // 255, 255, 255
            BLACK      = 13  //   0,   0,   0
        };

        /**
         * Main constructor
         * 
         * @param r_pin Pin to use for red value
         * @param g_pin Pin to use for green value
         * @param b_pin Pin to use for blue value
         */
        RGBLed(byte r_pin, byte g_pin, byte b_pin);

        /**
         * @brief Display the given colour
         * 
         * @param value Colour to display
         */
        void display(Colour colour);

        /**
         * @brief Getter for current colour displayed
         * 
         * @return Current colour displayed
         */
        RGBLed::Colour currentState();

        /**
         * @brief Evaluate if given value is bigger than the elapsed time since last update
         * 
         * @param value Time is ms
         * 
         * @return Whether given value is bigger than time elapsed since last update
         */
        bool updatedSinceMoreThan(unsigned long value);
    
    private:
        byte _r_pin;                // Pin for red value
        byte _g_pin;                // Pin for green value
        byte _b_pin;                // Pin for blue value

        unsigned long _lastUpdated; // Time of last colour update
        Colour _currentState;       // Current colour displayed

        /**
         * @brief Display the given colour
         * 
         * @param r_value Level of red to display
         * @param g_value Level of green to display
         * @param b_value Level of blue to display
         */
        void display(unsigned char r_value, unsigned char g_value, unsigned char b_value);
};

#endif
