#include "Button.h"

Button::Button(byte pin) {
    pinMode(pin, INPUT);
    this->_pin = pin;
}

bool Button::pressed() {
    return digitalRead(this->_pin) == HIGH;
}
