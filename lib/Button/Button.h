#ifndef BUTTON_H
#define BUTTON_H

#include <Arduino.h>

/**
 * @class Button
 * @brief Button class
 */
class Button {

    public:
        /**
         * Main constructor
         * 
         * @param pin Pin for the button
         */
        Button(byte pin);

        /**
         * Check whether the button
         * 
         * @return Whether the button is pressed
         */
        bool pressed();

    private:
        byte _pin; // Pin for the button
};

#endif
