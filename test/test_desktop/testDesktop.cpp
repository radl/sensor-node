#include <unity.h>

#include "UUID/UUIDTest.h"

void test() {
    UNITY_BEGIN();
    RUN_TEST(uuidStandardTest);
    RUN_TEST(uuidCustomTest);
    UNITY_END();
}

int main (int argc, char** argv) {
    test();
    return 0;
}
