#ifndef _UUID_TEST_H_
#define _UUID_TEST_H_

/**
 * @brief Test the creation of a standard UUID
 */
void uuidStandardTest();

/**
 * @brief Test the creation of a custom UUID
 */
void uuidCustomTest();

#endif
