#include <unity.h>
#include <utils/uuid/UUID.h>

#include "UUIDTest.h"

void uuidStandardTest() {
    TEST_ASSERT_EQUAL_STRING("00002A00-0000-1000-8000-00805F9B34FB", UUID("2A00", UUID::STANDARD_UUID).value());
}

void uuidCustomTest() {
    TEST_ASSERT_EQUAL_STRING("0000DS01-9DED-11EA-AB12-0800200C9A66", UUID("DS01", UUID::CUSTOM_UUID).value());
}
